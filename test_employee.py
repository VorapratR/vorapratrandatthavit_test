import unittest
import employee


class TestEmployeeBonus(unittest.TestCase):
    def test_give_1000_should_return_tax_1100(self):
        saraly = 1000
        expected = 77.00
        e = employee.Employee(saraly)

        def get_bonus():
            print('In Mock Function')
            return 100

        e.get_bonus = get_bonus

        self.assertEqual(expected, round(e.get_tax(), 2))


if __name__ == '__main__':
    unittest.main()
